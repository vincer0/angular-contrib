import React from "react";
import { DataGrid, GridActionsCellItem, useGridApiRef } from "@mui/x-data-grid";
import InfoIcon from "@mui/icons-material/Info";
import { useHistory } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { setCurrentContributor } from "../store/reducers/contributors";
import { useState } from "react";
import { getUsersInfoInChunk } from "../services/users";

const AcDataTable = () => {
  const apiRef = useGridApiRef();
  const history = useHistory();
  const rowData = useSelector((state) => state.rowData);
  const chunkedData = useSelector((state) => state.chunked);
  const dispatch = useDispatch();
  const [currentPage, setCurrentPage] = useState(0);
  const loading = useSelector((state) => state.loading);
  const columns = [
    { field: "id", headerName: "ID" },
    { field: "email", headerName: "Email", flex: 1 },
    {
      field: "contribs",
      headerName: "Contributions",
      sortable: true,
      flex: 1,
      type: "number",
    },
    {
      field: "follows",
      headerName: "Followers",
      type: "number",
      sortable: true,
      flex: 1,
    },
    {
      field: "gists",
      headerName: "Gists",
      sortable: true,
      type: "number",
      flex: 1,
    },
    {
      field: "repos",
      headerName: "Public repos",
      type: "number",
      sortable: true,
      flex: 1,
    },
    {
      field: "actions",
      type: "actions",
      getActions: (params) => [
        <GridActionsCellItem
          label="More info"
          icon={<InfoIcon></InfoIcon>}
          onClick={() => {
            dispatch(setCurrentContributor(params.row));
            history.push(`/contributor?name=${params.row.email}`);
          }}
        ></GridActionsCellItem>,
      ],
    },
  ];

  const handlePageChange = (event) => {
    setCurrentPage(event);
    getUsersInfoInChunk(event, chunkedData[event]);
  };

  return (
    <DataGrid
      apiRef={apiRef}
      rows={
        chunkedData && chunkedData.length > 0 ? chunkedData[currentPage] : []
      }
      columns={columns}
      onPageChange={handlePageChange}
      page={currentPage}
      loading={loading}
      pageSize={20}
      rowsPerPageOptions={[20]}
      rowCount={rowData && rowData.length > 0 ? rowData.length : 0}
      paginationMode="server"
      disableSelectionOnClick
    />
  );
};

export default AcDataTable;
