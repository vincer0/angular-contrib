import Axios from "axios";

const URL = "https://api.github.com/";

const PAT = process.env.REACT_APP_API_KEY;

const instance = Axios.create({
  baseURL: URL,
  headers: {
    Authorization: `token ${PAT}`,
  },
});

instance.interceptors.request.use(
  (config) => {
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

export default instance;
