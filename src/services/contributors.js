import Promise from "bluebird";
import { chunk } from "lodash";
import axios from "../axios";
import store from "../store";
import {
  setRowData,
  setChunked,
  setContributorsInRepos,
  setFetched,
} from "../store/reducers/contributors";
import { getUsersInfoInChunk } from "./users";

const getContributorsAllPages = async (repo) => {
  let page = 2;
  const contributorsFromAllPages = [];
  let lastData = [];

  do {
    lastData = await axios.get(
      `repos/angular/${repo}/contributors?per_page=100&anon=1&page=${page}`
    );
    contributorsFromAllPages.push(lastData.data);
    page++;
  } while (lastData.data.length > 1);

  return contributorsFromAllPages.flat();
};

export const getContributors = async (repos) => {
  const contributors = [];
  let fetched = 0;

  repos.forEach((repo, index) => {
    Promise.delay(200 * index).then(async () => {
      await axios
        .get(`repos/angular/${repo}/contributors?per_page=100&page=1&anon=1`)
        .then(async (response) => {
          if (response.headers.link) {
            contributors.push(
              (await getContributorsAllPages(repo)).concat(response.data)
            );
          } else {
            contributors.push(response.data);
          }
          fetched++;
          store.dispatch(setFetched(fetched));

          if (repos.length === fetched) {
            trimContributorsData(contributors.flat());
            store.dispatch(setContributorsInRepos(contributors));
          }
        });
    });
  });
};

const trimContributorsData = (contributors) => {
  const trimmed = contributors.map((contributor, index) => {
    return {
      id: index,
      email: contributor.email ? contributor.email : contributor.login,
      contribs: contributor.contributions,
    };
  });

  rankContributors(trimmed);
};

const rankContributors = (contributors) => {
  const countedContributions = [];
  contributors.forEach((contrib) => {
    if (typeof contrib === "object") {
      if (
        countedContributions.findIndex((e) => e.email === contrib.email) === -1
      ) {
        let contributions = 0;
        countedContributions.push(contrib);

        contributors.forEach((c) => {
          if (c.email === contrib.email) {
            contributions = contributions + c.contribs;
          }
        });

        const idx = countedContributions.indexOf(contrib);

        if (idx !== -1) {
          countedContributions[idx].contribs = contributions;
        }
      }
    }
  });

  parseDataToColumns(countedContributions);
};

const parseDataToColumns = (contributors) => {
  const parsedData = contributors.map((contributor, index) => {
    return {
      id: index,
      email: contributor.email ? contributor.email : contributor.login,
      contribs: contributor.contribs,
      follows: 0,
      gists: 0,
      repos: 0,
    };
  });

  const sorted = parsedData.sort((a, b) => b.contribs - a.contribs);

  store.dispatch(setRowData(sorted));

  chunkData(sorted);
};

const chunkData = (data, perChunk = 20) => {
  const chunked = chunk(data, perChunk);
  store.dispatch(setChunked(chunked));
  getUsersInfoInChunk(0, chunked[0]);
};
