import axios from "../axios";
import store from "../store";
import { updateChunk, setLoading } from "../store/reducers/contributors";
import Promise from "bluebird";

export const getUsersInfoInChunk = (chunkNumber, chunk) => {
  let newChunk = [];
  store.dispatch(setLoading(true));
  chunk.forEach((user, index) => {
    Promise.delay(200 * index).then(async () => {
      const newUser = await getUserInfo(user);
      newChunk.push(newUser);
      if (index === chunk.length - 1) {
        store.dispatch(updateChunk({ newChunk: newChunk, chunkNumber }));
        store.dispatch(setLoading(false));
      }
    });
  });
};

export const getUserInfo = (user) => {
  return axios
    .get(`users/${user.email}`)
    .then((response) => {
      return {
        ...user,
        follows: response.data.followers,
        gists: response.data.public_gists,
        repos: response.data.public_repos,
      };
    })
    .catch(() => {
      return user;
    });
};

export const getUserAllInfo = (username) => {
  return axios
    .get(`users/${username}`)
    .then((response) => response.data)
    .catch((error) => {
      return Promise.reject(error);
    });
};

export const getUserContributionsRepos = (contributor) => {
  const contributionsRepos = [];
  const repoList = store.getState().reposList;

  store.getState().contributorsInRepos.forEach((repo, index) => {
    if (Array.isArray(repo)) {
      const element = repo.find(
        (element) =>
          element.login === contributor.email ||
          element.email === contributor.email
      );
      if (element) {
        contributionsRepos.push({
          repoId: repoList[index].id,
          repoName: repoList[index].name,
          repoLink: repoList[index].html_url,
          repoContribs: element.contributions,
        });
      }
    }
  });
  return contributionsRepos;
};
