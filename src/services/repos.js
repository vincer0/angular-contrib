import axios from "../axios";
import { getContributors } from "./contributors";
import store from "../store";
import { setReposList } from "../store/reducers/contributors";

const getUserInfo = async (organizationName) => {
  if (!organizationName) {
    console.error("[GET ORGANIZATION INFO] - organization name not provided");
    return;
  }

  const response = await axios.get(`users/${organizationName}`);

  return response;
};

const getAllRepos = (owner) => {
  let numberOfRepos = 0;
  let numberOfPages = 0;
  const perPage = 30;
  const promises = [];
  const repos = [];

  getUserInfo(owner).then((response) => {
    numberOfRepos = response.data.public_repos;

    if (numberOfRepos > 0) {
      numberOfPages = Math.ceil(numberOfRepos / perPage);

      for (let i = 0; i < numberOfPages; i++) {
        promises.push(
          getReposPage(owner, perPage, i + 1).then((response) =>
            repos.push(response.data)
          )
        );
      }

      Promise.all(promises).then(() => {
        getContributors(repos.flat().map((repo) => repo.name));
        store.dispatch(setReposList(repos.flat()));
      });
    }
  });
};

const getReposPage = async (owner, perPage = 30, page = 1) => {
  if (!owner) {
    console.error("[GET REPOS] - owner not provided");
    return;
  }

  return await axios.get(
    `users/${owner}/repos?per_page=${perPage}&page=${page}&type=all`
  );
};

const getRepoInfo = (repoName) => {
  return axios.get(`repos/angular/${repoName}`);
};

export { getUserInfo, getAllRepos, getRepoInfo };
