import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import useQuery from "../hooks/useQuery";
import { getRepoInfo } from "../services/repos";
import { useSelector } from "react-redux";

import Box from "@mui/material/Box";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import Stack from "@mui/material/Stack";
import Typography from "@mui/material/Typography";
import Divider from "@mui/material/Divider";
import CircularProgress from "@mui/material/CircularProgress";

const RepoInfo = () => {
  const history = useHistory();
  const query = useQuery();
  const [loadingRepo, setLoadingRepo] = useState(false);
  const [repoInfo, setRepoInfo] = useState(null);
  const reposList = useSelector((state) => state.reposList);
  const contributorsInRepos = useSelector((state) => state.contributorsInRepos);

  useEffect(() => {
    setLoadingRepo(true);
    getRepoInfo(query.get("name")).then((response) => {
      setRepoInfo({
        fullName: response.data.full_name,
        ownerLogin: response.data.owner.login,
        watchers: response.data.watchers,
      });
      setLoadingRepo(false);
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const getContributorsOfRepo = () => {
    if (reposList.length > 0 && contributorsInRepos.length > 0) {
      const idxOfRepo = reposList.findIndex(
        (repo) => repo.name === query.get("name")
      );
      if (idxOfRepo !== -1) {
        const contributorsNames = contributorsInRepos[idxOfRepo].map(
          (c_repo) => {
            return c_repo.login ? c_repo.login : c_repo.email;
          }
        );

        return contributorsNames;
      }
    }

    return [];
  };

  return (
    <Box
      sx={{
        height: "calc(100vh - 128px)",
        marginTop: "32px",
      }}
    >
      <ArrowBackIcon onClick={() => history.goBack()}></ArrowBackIcon>
      <Typography variant="h4" align="center">
        {query.get("name")}
      </Typography>
      <Typography variant="h5" sx={{ mt: 4 }}>
        Basic info:
      </Typography>
      <Divider orientation="horizontal" flexItem />
      {!loadingRepo && repoInfo !== null ? (
        <>
          <Stack direction="row" justifyContent="space-between" sx={{ mt: 4 }}>
            <div>{`Full name: ${repoInfo.fullName}`}</div>
            <div>{`Owner: ${repoInfo.ownerLogin}`}</div>
            <div>{`Watchers: ${repoInfo.watchers}`}</div>
          </Stack>
        </>
      ) : (
        <CircularProgress color="inherit" />
      )}
      <Typography variant="h5" sx={{ mt: 4 }}>
        Contributors:
      </Typography>
      <Divider orientation="horizontal" flexItem />
      {getContributorsOfRepo().length > 0 ? (
        getContributorsOfRepo().map((name, index) => (
          <div key={index} style={{ marginTop: 8 }}>
            {name}
          </div>
        ))
      ) : (
        <CircularProgress color="inherit" />
      )}
    </Box>
  );
};

export default RepoInfo;
