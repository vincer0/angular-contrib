import React from "react";
import { useSelector } from "react-redux";

import Box from "@mui/material/Box";
import AcDataTable from "../components/AcDataTable";
import Backdrop from "@mui/material/Backdrop";
import CircularProgress from "@mui/material/CircularProgress";

const Contributors = () => {
  const allReposCount = useSelector((state) => state.reposList.length);
  const fetched = useSelector((state) => state.fetched);
  const loading = useSelector((state) => state.loading);

  return (
    <>
      <Box
        sx={{
          height: "calc(100vh - 128px)",
          marginTop: "32px",
        }}
      >
        <AcDataTable></AcDataTable>
      </Box>
      <Backdrop sx={{ color: "#fff" }} open={loading}>
        <CircularProgress color="inherit" />
        {fetched !== allReposCount && (
          <div>{`${fetched} / ${allReposCount}`}</div>
        )}
      </Backdrop>
    </>
  );
};

export default Contributors;
