import React from "react";
import { useHistory } from "react-router-dom";
import { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { getUserAllInfo } from "../services/users";
import { setCurrentContributor } from "../store/reducers/contributors";
import { getUserContributionsRepos } from "../services/users";
import useQuery from "../hooks/useQuery";

import Box from "@mui/material/Box";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import Avatar from "@mui/material/Avatar";
import Stack from "@mui/material/Stack";
import Typography from "@mui/material/Typography";
import Divider from "@mui/material/Divider";
import CircularProgress from "@mui/material/CircularProgress";
import Button from "@mui/material/Button";

const Contributor = () => {
  const history = useHistory();
  const storedContributor = useSelector((state) => state.currentContributor);
  const loading = useSelector((state) => state.loading);
  const contributorsInRepos = useSelector((state) => state.contributorsInRepos);
  const rowData = useSelector((state) => state.rowData);
  const [contributor, setContributor] = useState(null);
  const [userDetails, setUserDetails] = useState();
  const [moreInfoLoading, setMoreInfoLoading] = useState();
  const [basicInfoLoading, setBasicInfoLoading] = useState();
  const dispatch = useDispatch();
  const [contributedRepos, setContributedRepos] = useState();
  const query = useQuery();

  useEffect(() => {
    if (storedContributor === null && query.get("name") !== "") {
      setBasicInfoLoading(true);
      getUserAllInfo(query.get("name"))
        .then((response) => {
          setContributor({
            email: response.login,
            follows: response.followers,
            repos: response.public_repos,
            gists: response.public_gists,
          });
          setUserDetails({
            avatar_url: response.avatar_url,
            created_at: response.created_at,
            location: response.location,
            company: response.company,
            blog: response.blog,
          });
          dispatch(setCurrentContributor(contributor));
          setBasicInfoLoading(false);
        })
        .catch(() => {
          history.push("/");
        });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (storedContributor !== null) {
      setMoreInfoLoading(true);
      getUserAllInfo(storedContributor.email)
        .then((response) => {
          setContributor(storedContributor);
          setUserDetails(response);
          setMoreInfoLoading(false);
        })
        .catch(() => {
          setContributor(storedContributor);
          setUserDetails({});
          setMoreInfoLoading(false);
        });
    }
  }, [storedContributor]);

  useEffect(() => {
    if (!loading && contributorsInRepos.length > 0 && contributor !== null) {
      setContributedRepos(getUserContributionsRepos(contributor));
    }
  }, [contributorsInRepos.length, loading, contributor]);

  useEffect(() => {
    if (rowData.length > 0 && contributor !== null) {
      const { contribs } = rowData.find(
        (data) => data.email === contributor.email
      );
      if (contribs) {
        setContributor({ ...contributor, contribs });
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [rowData]);

  const handleOnClickBack = () => {
    history.push("/");
    dispatch(setCurrentContributor(null));
  };

  return (
    <Box
      sx={{
        height: "calc(100vh - 64px)",
        marginTop: "64px",
      }}
    >
      <div>
        <ArrowBackIcon onClick={handleOnClickBack}></ArrowBackIcon>
        <Stack
          direction="column"
          spacing={4}
          divider={<Divider orientation="horizontal" flexItem />}
        >
          <Typography variant="h4">Basic Info:</Typography>
          {!basicInfoLoading && contributor !== null ? (
            <>
              <Stack direction="row" spacing={2} alignItems="center">
                {userDetails && <Avatar src={userDetails.avatar_url}></Avatar>}
                <Typography variant="body1">{contributor.email}</Typography>
              </Stack>
              <div>
                <Stack
                  direction="row"
                  spacing={4}
                  justifyContent="space-between"
                >
                  <div>
                    <Typography variant="body1">All contributions:</Typography>
                    {!loading ? (
                      <div>{contributor.contribs}</div>
                    ) : (
                      <CircularProgress color="inherit" />
                    )}
                  </div>
                  <div>
                    <Typography variant="body1">Followers:</Typography>
                    <div>{contributor.follows}</div>
                  </div>
                  <div>
                    <Typography variant="body1">Public repos:</Typography>
                    <div>{contributor.repos}</div>
                  </div>
                  <div>
                    <Typography vairant="body1">Gists:</Typography>
                    <div>{contributor.gists}</div>
                  </div>
                </Stack>
              </div>
            </>
          ) : (
            <CircularProgress color="inherit" />
          )}
          <Typography variant="h4">More Info:</Typography>
          <>
            {!moreInfoLoading && userDetails ? (
              <div>
                <Stack
                  direction="row"
                  spacing={4}
                  justifyContent="space-between"
                >
                  <div>
                    <Typography variant="body1">Created At</Typography>
                    <div>{userDetails.created_at}</div>
                  </div>
                  <div>
                    <Typography variant="body1">Company</Typography>
                    <div>{userDetails.company}</div>
                  </div>
                  <div>
                    <Typography variant="body1">Location:</Typography>
                    <div>{userDetails.location}</div>
                  </div>
                  <div>
                    <Typography vairant="body1">Blog</Typography>
                    <a href={userDetails.blog}>{userDetails.blog}</a>
                  </div>
                </Stack>
              </div>
            ) : (
              <CircularProgress color="inherit" />
            )}
          </>
          <div>
            <Typography variant="h4" sx={{ mb: 2 }}>
              Contributed to:
            </Typography>
            {!loading ? (
              <Stack
                direction="column"
                spacing={4}
                divider={<Divider orientation="horizontal" flexItem />}
              >
                {contributedRepos &&
                  contributedRepos.map((repo) => (
                    <div key={repo.repoId}>
                      <a href={repo.repoLink}>{repo.repoName}</a>
                      <div>{`Contributions: ${repo.repoContribs}`}</div>
                      <Button
                        onClick={() =>
                          history.push(`/repository?name=${repo.repoName}`)
                        }
                        variant="text"
                      >
                        More info
                      </Button>
                    </div>
                  ))}
              </Stack>
            ) : (
              <CircularProgress color="inherit" />
            )}
          </div>
        </Stack>
      </div>
    </Box>
  );
};

export default Contributor;
