import { configureStore } from "@reduxjs/toolkit";
import contributorsSlice from "./reducers/contributors";

export default configureStore({
  reducer: contributorsSlice,
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: false,
    }),
});
