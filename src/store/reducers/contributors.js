import { createSlice } from "@reduxjs/toolkit";

export const contributorsSlice = createSlice({
  name: "contributors",
  initialState: {
    rowData: [],
    currentContributor: null,
    chunked: [],
    loading: false,
    contributorsInRepos: [],
    reposList: [],
    fetched: 0,
    status: "",
  },
  reducers: {
    setRowData: (state, action) => {
      state.rowData = action.payload;
    },
    setCurrentContributor: (state, action) => {
      state.currentContributor = action.payload;
    },
    setChunked: (state, action) => {
      state.chunked = action.payload;
    },
    setLoading: (state, action) => {
      state.loading = action.payload;
    },
    updateChunk: (state, action) => {
      state.chunked[action.payload.chunkNumber] = action.payload.newChunk;
    },
    setContributorsInRepos: (state, action) => {
      state.contributorsInRepos = action.payload;
    },
    setReposList: (state, action) => {
      state.reposList = action.payload;
    },
    setFetched: (state, action) => {
      state.fetched = action.payload;
    },
    setStatus: (state, action) => {
      state.status = action.payload;
    },
  },
});

export const {
  setRowData,
  setCurrentContributor,
  setChunked,
  setLoading,
  updateChunk,
  setContributorsInRepos,
  setReposList,
  setStatus,
  setFetched,
} = contributorsSlice.actions;

export default contributorsSlice.reducer;
