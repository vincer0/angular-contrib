import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { setLoading } from "./store/reducers/contributors";
import { getAllRepos } from "./services/repos";

import "./App.css";
import Container from "@mui/material/Container";
import AcAppBar from "./components/AcAppBar";
import Contributors from "./views/Contributors";
import Contributor from "./views/Contributor";
import RepoInfo from "./views/RepoInfo";

function App() {
  const dispatch = useDispatch();
  const rowData = useSelector((state) => state.rowData);

  useEffect(() => {
    if (rowData.length > 0) {
      return;
    }
    dispatch(setLoading(true));
    getAllRepos("angular");
  }, [rowData, dispatch]);

  return (
    <div className="app">
      <AcAppBar></AcAppBar>
      <Container>
        <Router>
          <Switch>
            <Route exact path="/">
              <Contributors></Contributors>
            </Route>
            <Route exact path="/contributor">
              <Contributor></Contributor>
            </Route>
            <Route exact path="/repository">
              <RepoInfo></RepoInfo>
            </Route>
          </Switch>
        </Router>
      </Container>
    </div>
  );
}

export default App;
